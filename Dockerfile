# Use the official HashiCorp Vault image as a parent image
FROM hashicorp/vault:latest

# Install necessary packages for downloading the plugin (Alpine-based)
RUN apk add --no-cache wget unzip

# Download and install the 1Password plugin
USER vault
WORKDIR /vault/plugins
RUN wget https://github.com/1Password/vault-plugin-secrets-onepassword/releases/download/v1.1.0/vault-plugin-secrets-onepassword_1.1.0_linux_amd64.zip && \
    unzip vault-plugin-secrets-onepassword_1.1.0_linux_amd64.zip && \
    rm vault-plugin-secrets-onepassword_1.1.0_linux_amd64.zip && \
    cp vault-plugin-secrets-onepassword_v1.1.0 op-connect && \
    chmod 755 op-connect

# Add the registration script
COPY scripts/register_op_plugin.sh /home/vault/register_op_plugin.sh

USER root
