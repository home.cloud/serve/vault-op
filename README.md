# HashiCorp Vault 1Password Sync

This project sets up a HashiCorp Vault service integrated with the 1Password Vault plugin (`op-connect`) using Docker Compose.

## Prerequisites

- Docker
- Docker Compose

## Setup

### Clone the Repository

```bash
git clone git@gitlab.com:home.cloud/serve/vault-op.git
cd vault-op
cp .env.template .env # and update with your secrets
```

### Build and Run the Services

```bash
docker compose up --build -d
```

### Register the 1Password Plugin

After the services are up, we need to manually run the following command to register the 1Password plugin with Vault:

```bash
docker compose exec vault /tmp/register_op_plugin.sh
```

## Configuration

### Environment Variables

You can set the 1Password Connect API token as an environment variable before running Docker Compose:

```bash
export OP_CONNECT_TOKEN=your_access_token_here
```

## Troubleshooting

If you encounter any issues, check the Vault logs:

```bash
docker-compose logs vault
```

## Contributing

Feel free to open issues or submit pull requests.

## License

This project is licensed under the ATM License.
