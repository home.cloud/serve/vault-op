ui = true
disable_mlock = true
api_addr = "http://0.0.0.0:8200"

log_level = "debug"

listener "tcp" {
  address = "0.0.0.0:8200"
  "tls_disable" = 1
}

storage "file" {
  path = "/vault/file"
}

plugin_directory = "/vault/plugins"