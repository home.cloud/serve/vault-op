#!/bin/sh

set -x

# Calculate the SHA-256 checksum of the plugin binary
PLUGIN_SHA256=$(sha256sum /vault/plugins/op-connect | cut -d ' ' -f1)

# Register the plugin with Vault
vault plugin register -sha256=$PLUGIN_SHA256 secret op-connect

# Enable the plugin at a specific path
vault secrets enable -path="op" op-connect

# Generate the JSON file using the environment variables
cat <<EOL > op-connect-config.json
{
	"op_connect_host": "$OP_CONNECT_HOST",
	"op_connect_token": "$OP_CONNECT_TOKEN"
}
EOL

# Load the configuration for the vault op plugin
vault write op/config @op-connect-config.json

set +x