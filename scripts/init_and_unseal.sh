#!/bin/bash

set -x

# Initialize Vault and capture the keys and root token
INIT_OUTPUT=$(docker-compose exec -T vault vault operator init -key-shares=1 -key-threshold=1 -format=json)

# Extract the unseal key and root token from the JSON output
UNSEAL_KEY=$(echo $INIT_OUTPUT | jq -r '.unseal_keys_b64[]')
ROOT_TOKEN=$(echo $INIT_OUTPUT | jq -r '.root_token')

# Unseal Vault using the unseal key
docker-compose exec -T -e VAULT_TOKEN=$ROOT_TOKEN vault vault operator unseal $UNSEAL_KEY

# Print root token
echo $ROOT_TOKEN

set -x